#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#define ROW_A 2
#define COL_A 2

#define ROW_B 2
#define COL_B 2

#define NB_THREADS 12

void fill_matrix(int *M, int ROW, int COL)
{
    for (int i = 0; i < ROW * COL; i++)
    {
        M[i] = i + 1;
    }
}

void print_matrix(int *M, int ROW, int COL)
{
    for (int n = 0; n < (ROW * COL); n++)
    {
        int row_index, col_index;
        row_index = (n / COL);
        col_index = (n % COL);

        printf("%s", (n % COL == 0) ? "\n" : "");
        printf(" |(%d,%d)=%7d", row_index, col_index, M[n]);
    }
    printf("\n");
}

int main(void)
{
    omp_set_num_threads(3);

    double time_start, time_end;

    int *A;
    int *B;
    int *C;

    A = calloc((ROW_A * COL_A), sizeof(int));
    B = calloc((ROW_B * COL_B), sizeof(int));
    C = calloc((ROW_A * COL_B), sizeof(int));

    if (A && B && C)
    {
        omp_set_num_threads(NB_THREADS);

        fill_matrix(A, ROW_A, COL_A);
        fill_matrix(B, ROW_B, COL_B);

        if (COL_A == ROW_B)
        {
            time_start = omp_get_wtime();
#pragma omp parallel
            {
                int numero_thread = omp_get_thread_num();

                for (int n = numero_thread; n < (ROW_A * COL_B); n += NB_THREADS)
                {
                    int row_index, col_index;
                    row_index = (n / COL_B);
                    col_index = (n % COL_B);

                    for (int i = 0; i < COL_A; i++)
                    {
                        int index_a, index_b;
                        index_a = row_index * COL_A + i;
                        index_b = i * COL_B + col_index;
                        C[n] += A[index_a] * B[index_b];
                    }
                }
            }
            time_end = omp_get_wtime();

            printf("--> Finie en : %f s \n", time_end - time_start);

            printf("\nMatrice A : \n");
            print_matrix(A, ROW_A, COL_A);
            printf("\nMatrice B : \n");
            print_matrix(B, ROW_B, COL_B);
            printf("\nMatrice C : \n");
            print_matrix(C, ROW_A, COL_B);
        }
        free(A);
        free(B);
        free(C);
    }
    return EXIT_SUCCESS;
}
