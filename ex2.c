#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define NB_THREADS 4

int main(int argc, char *argv[])
{
    long i,
        nombre_iteration = 10E10;
    int current_thread, nb_threads = NB_THREADS;

    omp_set_num_threads(nb_threads);

    long double step = 1.0 / nombre_iteration,
           partial_res = 0,
           tmp_x,
           tmp_x_1,
           tmp_y,
           tmp_y_1,
           aire,
           final_res,
           time_start,
           time_end;

    time_start = omp_get_wtime();
#pragma omp parallel private(i, tmp_x, tmp_x_1, tmp_y, tmp_y_1, aire) reduction(+ \
                                                                                : partial_res)
    {
        current_thread = omp_get_thread_num();

        for (i = current_thread; i < nombre_iteration; i += nb_threads)
        {
            tmp_x = step * i;
            tmp_x_1 = step * (i + 1);

            tmp_y = (1 / (1 + tmp_x * tmp_x));
            tmp_y_1 = (1 / (1 + tmp_x_1 * tmp_x_1));

            aire = (tmp_y_1 * step) + ((tmp_y - tmp_y_1) * step) / 2;

            partial_res += aire;
        }
    }
    final_res = 4 * partial_res;
    time_end = omp_get_wtime();
    printf("-- Aproximation de Pi (Thread : %d | Temps : %3.2Lf s)--\nPi ~ %.15Lf \n", nb_threads, (time_end - time_start), final_res);
    return EXIT_SUCCESS;
}
