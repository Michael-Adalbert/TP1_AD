#include <stdio.h>
#include <omp.h>

int main(void)
{
#pragma omp parallel
    {
        printf("Thread Courant : %d \nNumbre de Processeur : %d \nNombre de Thread dans le Groupe : %d \nNombre max de Thread : %d \nParallel : %s \n\n ",
               omp_get_thread_num(),
               omp_get_num_procs(),
               omp_get_num_threads(),
               omp_get_max_threads(),
               (omp_in_parallel()) ? "OUI" : "NON");
    }
}